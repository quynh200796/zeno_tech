<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class HomeController extends Controller
{
    public function getIndex() {
    
    	return redirect()->route('RouteIndex');
    }

     public function changeLanguage($language)
	{
	    \Session::put('website_language', $language);
	    return redirect()->back();
	}
}
