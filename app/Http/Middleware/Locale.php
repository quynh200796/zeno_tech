<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Lang;
use App;
use Carbon;
class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // var_dump(Session::get('locale'));die();
         if (!Session::has(\"locale") {
              Session::put(\"locale", config(\'app.locale'));
          }

          Lang::setLocale(Session::get(\'locale'));

          return $next($request);
    }


}
