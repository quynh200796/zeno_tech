<?php
	return [
		"home" => "Trang chủ",
		"procedure" => "Quy trình",
		"product" => "Sản phẩm",
		"price" => "Báo giá",
		"contact" => "Liên hệ",
		"vietnamese" => "Tiếng Việt",
		"english" => "Tiếng Anh",
		"service" => "Dịch vụ",
		"commit" => "Cam kết" ,
		"partner" => "Đối tác",
	];
?>