<?php
	return [
		"home" => "Home",
		"procedure" => "Procedure",
		"product" => "Product",
		"price" => "Prices",
		"contact" => "Contact",
		"vietnamese" => "Vietnamese",
		"english" => "English",
		"service" => "Services",
		"commit" => "commitment" ,
		"partner" => "Partners",
	];
?>