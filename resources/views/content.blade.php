 <section class="banner">
        <div class="pic-banner">
            <img src="IMG/banner.png" alt="banner">
        </div>
    </section>
    <section>
        <div class="service">
            <div id="particles-js"></div>
            <div class="container">
                <div class="title-service">
                    {{ trans('words.service')}}
                </div>
                <div class="all-service">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="pic-service">
                                <img src="IMG/service 1 .png" alt="service1">
                            </div>
                            <div class="text-service">
                                Lorem Ipsum’ are the first two words of a classic piece of dummy text. Such filler text is used when the actual text is not available yet or when creating a brand new layout. Because the text is in Latin, people won’t be distracted by it and can focus on evaluating the design itself.
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="pic-service">
                                <img src="IMG/service 2.png" alt="service1">
                            </div>
                            <div class="text-service">
                                Lorem Ipsum’ are the first two words of a classic piece of dummy text. Such filler text is used when the actual text is not available yet or when creating a brand new layout. Because the text is in Latin, people won’t be distracted by it and can focus on evaluating the design itself.
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="pic-service">
                                <img src="IMG/service 3.png" alt="service1">
                            </div>
                            <div class="text-service">
                                Lorem Ipsum’ are the first two words of a classic piece of dummy text. Such filler text is used when the actual text is not available yet or when creating a brand new layout. Because the text is in Latin, people won’t be distracted by it and can focus on evaluating the design itself.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="commitment">
            <div class="container">
                <div class="title-commitment">
                   {{ trans('words.commit')}}
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="all-commitment">
                            <div class="icon-commitment">
                                <img src="IMG/tiendo.png" alt="progress">
                            </div>
                            <div class="slogan-commitment">
                                Tiến độ
                            </div>
                            <div class="text-commitment">
                                Chúng tôi luôn cam kết hoàn thành công việc sớm hơn dự kiến
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="all-commitment">
                            <div class="icon-commitment">
                                <img src="IMG/price.png" alt="price">
                            </div>
                            <div class="slogan-commitment">
                                Giá cả
                            </div>
                            <div class="text-commitment">
                                Chúng tôi luôn có các gói ưu đãi cho khách hàng theo từng đợt
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="all-commitment">
                            <div class="icon-commitment">
                                <img src="IMG/service.png" alt="service">
                            </div>
                            <div class="slogan-commitment">
                                Dịch vụ
                            </div>
                            <div class="text-commitment">
                                Cung cấp giải pháp marketing tổng thể cho doanh nghiệp
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="all-commitment">
                            <div class="icon-commitment">
                                <img src="IMG/support.png" alt="support">
                            </div>
                            <div class="slogan-commitment">
                                Hỗ trợ
                            </div>
                            <div class="text-commitment">
                                Luôn hỗ trợ - tư vấn giải quyết mọi vấn đề của khách hàng 24/7
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="all-partner">
        <div class="container">
            <div class="partner">
                <div class="title-partner text-center mb-5">
                    <h1>{{ trans('words.partner')}}</h1>
                </div>
                <div class="partners">
                    <div class="logo-partner">
                        <a href="#"><img src="IMG/Asset 1.png" alt="tvnet"></a>
                    </div>
                    <div class="logo-partner">
                        <a href="#"><img src="IMG/1946.png" alt="1946"></a>
                    </div>
                    <div class="logo-partner">
                        <a href="#"><img src="IMG/AR.png" alt="AR"></a>
                    </div>
                    <div class="logo-partner">
                        <a href="#"><img src="IMG/fantv1.png" alt="fantv1"></a>
                    </div>
                    <div class="logo-partner">
                        <a href="#"><img src="IMG/fantv2.png" alt="fantv2"></a>
                    </div>
                </div>
                <div class="text-partner">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                </div>
            </div>
        </div>
    </section>