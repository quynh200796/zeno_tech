<div class="header">
            <div class="border-bottom bbottom">
                <div class="container">
                    <div class="all-menu">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="logo">
                                    <a href="#"><img src="IMG/logo.png" alt="" ></a>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="nav">
                                        <div class="menu-icon">
                                            <i class="fa fa-bars fa-2x"></i>
                                        </div>
                        
                                    <ul class="menu">
                                        <li><a href="#"class="custom">{{ trans('words.home') }}</a></li>
                                        <li><a href="#"class="custom">{{ trans('words.procedure') }}</a></li>
                                        <li><a href="#"class="custom">{{ trans('words.product') }}</a></li>
                                        <li><a href="#"class="custom">{{ trans('words.price') }}</a></li>
                                        <li><a href="#"class="custom">{{ trans('words.contact') }}</a></li>
                                    @if (\Session::get('locale') == 'en')
                                          <li class="all-language" >
                                             <a href="{{URL::asset('')}}language/en" class="language">{{ trans('words.english') }}<img src="IMG/quoc_ky-01.jpg" alt="England">
                                                </a>
                                            <ul class="subnav">
                                                <li class="down ">
                                                    <a href="{{URL::asset('')}}language/vi" >{{ trans('words.vietnamese') }}<img src="IMG/quoc_ky-02.jpg" alt="Vietnamese"></a>
                                                </li>
                                            </ul>
                                            </li>
                                        @else
                                        <li class="all-language" >
                                            <a href="{{URL::asset('')}}language/vi" class="language">{{ trans('words.vietnamese') }}<img src="IMG/quoc_ky-02.jpg" alt="Vietnamese"></a>
                                        <ul class="subnav">
                                            <li class="down ">
                                                <a href="{{URL::asset('')}}language/en">{{ trans('words.english') }}<img src="IMG/quoc_ky-01.jpg" alt="England">
                                                </a>
                                            </li>
                                    </ul>
                                        </li>
                                    @endif
                                   
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>