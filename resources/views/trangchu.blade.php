<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zeno5 – Giải pháp phát triển cho doanh nghiệp trong thời đại công nghệ</title>
    <link rel="shortcut icon" type="image/png" href="IMG/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="Lib/Bootstrap 4/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="Lib/font-awesome-4.7.0/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="Lib/jquery321/jquery_321/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="Lib/Bootstrap 4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="Lib/particles.js-master/particles.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>
<body>
    <header>
        @include('header')
    </header>   
        @include('content')
    <footer>
        @include('footer')
    </footer>
</body>
</html>

