<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','HomeController@getIndex');

Route::get('/',['as'=>'RouteIndex', function () {
	if(Session::has('locale')){
		App::setlocale(Session::get('locale'));
	}
    return view('trangchu');
}]);

Route::get('language/{locale}',function($locale){
	Session::put('locale', $locale);
	return redirect()->back();
});